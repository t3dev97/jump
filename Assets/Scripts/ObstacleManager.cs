﻿using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public static ObstacleManager api;
    public GameObject obstacle;
    public List<GameObject> listObsHide = new List<GameObject>();
    public List<GameObject> listObsShow = new List<GameObject>();
    public Vector3 posDefault;
    private GameObject obstacleStayingScreen;
    private void Awake()
    {
        if (api == null) api = this;
    }
    private void Start()
    {
        GameObject obj;
        posDefault = transform.position;
        for (int i = 0; i < 5; i++)
        {
            obj = Instantiate(obstacle);
            obj.SetActive(false);
            obj.transform.position = posDefault;
            obj.transform.SetParent(transform);
            listObsHide.Add(obj);
        }
    }
    public GameObject CreateObstacle()
    {
        if (listObsHide != null && listObsHide.Count > 0)
        {
            listObsShow.Add(listObsHide[0]);
            listObsHide.Remove(listObsHide[0]);
            return listObsShow[0];
        }
        else
            return null;
    }
    void Update()
    {
        if (GameManager.Instance.mGameState == GameState.Playing)
        {
            // find obstacle object in screen
            obstacleStayingScreen = GameObject.FindGameObjectWithTag("Obstacle");
            // if screen not any obstacle then we will create a obstacle at this position.
            if (obstacleStayingScreen == null)
            {
                // case 1:
                CreateObstacle().SetActive(true);
                //Instantiate(obstacle);

                // case 2:
                //Instantiate(obstacle, transform.position, Quaternion.identity);
            }
        }
    }
}
